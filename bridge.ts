﻿class Bridge {
    private static internalInstance: Bridge = null;
    public static get instance() {

        if (Bridge.internalInstance == null)
            Bridge.internalInstance = new Bridge();

        return Bridge.internalInstance.scraper;
    }
    constructor() {
        // assert a constructor as private
        if (Bridge.internalInstance != null)
            console.error("The constructor of Bridge has been called by outside. Do not new() a Singleton Class directly.");
    }

    private _scraper: BridgeScraper = new BridgeScraper();
    public get scraper() {
        return this._scraper;
    }
}