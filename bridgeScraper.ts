﻿class BridgeScraper {
    public initialize(callback:()=>void) {
        callback();
    }

    public startGame(callback:()=>void) {
        callback();
    }

    public updateLoadProgress(persent: number) {
    }

    public quit() {
    }

    public set onPaused(callback:()=>void) {
    }

    public set onResumed(callback:()=>void) {
    }


    // ------------------------------- base information
    public get playerID() {
        return "1234567890";
    }

    public get playerName() {
        return "player-name";
    }

    public get playerIconURL() {
        return "player-icon-url";
    }
    public get entryPointString() {
        return "entry-point-string";
    }
    public get entryPointData() {
        return {};
    }
    public get locale() {
        return "locale";
    }


    // ----------------------- save data
    public loadPersistentData(
        tag: string,
        callback: (result: any) => void) {
        callback(null);
    }

    public savePersistentData(
        tag: string,
        payload: Object,
        callback: () => void) {
        callback();
    }


    // ------------------------ context (chat group)
    public joinContext(
        callback: () => void,
        failureCallback: (error) => void = ()=>{}
    ) {
        callback();
    }

    public postContext(
        message:string,
        optionalData:Object,
        callback: () => void,
        failureCallback: (error) => void = ()=>{}
    ) {
        callback();
    }

    public getContextMembers(
        callback: (members:Array<SocialAccount>) => void
    ) {
        callback(new Array<SocialAccount>());
    }

    public getContextRanking(
        callback: (rankers:Array<ScoreRanking>) => void,
        rankingTitle:string = "ranking"
    ) {
        callback(new Array<ScoreRanking>());
    }
    
    public getContextMyRanking(
        callback: (rank:ScoreRanking) => void,
        rankingTitle:string = "ranking"
    ) {
        callback(new ScoreRanking(
            -1,
            0,
            new SocialAccount(
                Bridge.instance.playerID,
                Bridge.instance.playerName,
                Bridge.instance.playerIconURL
            )
        ));
    }

    public setContextRankingScore(
        score:number,
        optionalData:Object,
        callback: () => void,
        failureCallback: (error) => void = ()=>{},
        rankingTitle:string = "ranking"
    ) {
        callback();
    }

    public get isSoloMode() {
        return true;
    }

    // --------------------------------- social
    public getRelatedAccounts(
        callback: (accounts:Array<SocialAccount>) => void
    ) {
        callback(new Array<SocialAccount>());
    }

    public share(
        message:string,
        optionalData:Object = {
            base64Image : "",
            data : {}
        },
        callback: () => void = () => {},
        failureCallback: (error) => void = error=>{}
    ) {
        callback();
    }

    public invite(
        message:string,
        optionalData:Object = {
            base64Image : "",
            data : {}
        },
        callback: () => void = () => {},
        failureCallback: (error) => void = error=>{}
    ) {
        callback();
    }

    public runAnotherGame(
        tag:string
    ) {

    }

    public createShortCut(
        callback:(created:boolean)=>void = created=>{}
    ) {
        callback(true);
    }

    public enableMessangerBot(
        callback:(enabled:boolean)=>void = enabled=>{}
    ) {
        callback(true);
    }

    public getRanking(
        startRank:number,
        endRank:number,
        callback: (rankers:Array<ScoreRanking>) => void,
        rankingTitle:string = "ranking"
    ) {
        callback(new Array<ScoreRanking>());
    }

    public getMyRanking(
        callback: (rank:ScoreRanking) => void,
        rankingTitle:string = "ranking"
    ) {
        callback(new ScoreRanking(
            -1,
            0,
            new SocialAccount(
                Bridge.instance.playerID,
                Bridge.instance.playerName,
                Bridge.instance.playerIconURL
            )
        ));
    }

    public setRankingScore(
        score:number,
        optionalData:Object,
        callback: () => void,
        failureCallback: (error) => void = ()=>{},
        rankingTitle:string = "ranking"
    ) {
        callback();
    }

    public getMatchedPlayers(
        tag: string,
        optionalData:Object,
        callback:(players:Array<SocialAccount>)=>void
    ) {
        callback(new Array<SocialAccount>());
    }

    // --------------------------------- ads
    public preloadAds(
        tag: string,
        optionalData: Object,
        callback: ()=>void
    ) {
        callback();
    }

    public showAds(
        tag: string,
        optionalData: Object,
        callback: ()=>void
    ) {
        callback();
    }

    // --------------------------------- tracking
    public log(
        tag: string,
        optionalData: Object
    ) {
    }
}

class SocialAccount {
    constructor(
        public playerId: string,
        public playerName: string,
        public playerIconUrl: string
    ) {

    }
}

class ScoreRanking {
    constructor(
        public rank:number,
        public score:number,
        public account: SocialAccount,
        public data: Object = {}
    ) {

    }
}